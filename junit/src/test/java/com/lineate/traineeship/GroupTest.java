package com.lineate.traineeship;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class GroupTest {
    static Group group;
    static User user;

    @BeforeAll
    static void init() {
        ServiceFactory factory = new ServiceFactory();
        UserService userService = factory.createUserService();

        String groupName = "group";
        String userName = "username";

        List<Permission> permissions = new ArrayList<>();
        permissions.add(Permission.write);
        permissions.add(Permission.read);

        group = userService.createGroup(groupName, permissions);
        user = userService.createUser(userName, group);
    }

    @Test
    void getNameTest() {
        assertAll(
                () -> assertNotNull(group.getName()),
                () -> assertFalse(group.getName().isEmpty())
        );
    }

    @Test
    void getUserTest() {
        assertFalse(group.getUsers().contains(user));
    }

    @Test
    void addUserTest() {
        group.addUser(user);
        assertTrue(group.getUsers().contains(user));
    }

    @Test
    void getPermissionsTest() {
        assertAll(
                () -> assertNotNull(group.getPermissions()),
                () -> assertFalse(group.getPermissions().isEmpty())
        );
    }
}