package com.lineate.traineeship;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class EntityServiceTest {
    static EntityService entityService;
    static UserService userService;
    static EntityRepository repository;

    @BeforeAll
    static void init() {
        ServiceFactory factory = new ServiceFactory();
        entityService = factory.createEntityService(repository);
        userService = factory.createUserService();
    }

    @Test
    void createEntityTest() {
        String groupName = "group";
        List<Permission> permissions = new ArrayList<>();
        permissions.add(Permission.write);
        permissions.add(Permission.read);

        String userName = "username";

        String entityName = "entityName";
        String entityValue = "entityValue";

        Group userGroup = userService.createGroup(groupName, permissions);
        User user = userService.createUser(userName, userGroup);
        boolean isCreated = entityService.createEntity(user, entityName, entityValue);
        Entity createdEntity = repository.get(entityName);

        assertAll(
                () -> assertTrue(isCreated),
                () -> assertNotNull(createdEntity),
                () -> assertEquals(entityName, createdEntity.getName()),
                () -> assertTrue(createdEntity.getGroups().containsAll(user.getGroups())),
                () -> assertEquals(user, createdEntity.getOwner()),
                () -> assertEquals(entityValue, createdEntity.getValue()));
    }

    @Test
    void getEntityValueTest() {
        String groupName = "group";
        List<Permission> permissions = new ArrayList<>();
        permissions.add(Permission.write);
        permissions.add(Permission.read);

        String userName = "username";

        String entityName = "entityName";
        String entityValue = "entityValue";

        Group userGroup = userService.createGroup(groupName, permissions);
        User user = userService.createUser(userName, userGroup);

        assertAll(
                () -> assertTrue(entityService.createEntity(user, entityName, entityValue)),
                () -> assertEquals(entityValue, entityService.getEntityValue(user, entityName))
        );
    }

    @Test
    void updateEntityTest() {
        String groupName = "group";
        List<Permission> permissions = new ArrayList<>();
        permissions.add(Permission.write);
        permissions.add(Permission.read);

        String userName = "username";

        String entityName = "entityName";
        String entityValue = "entityValue";

        Group userGroup = userService.createGroup(groupName, permissions);
        User user = userService.createUser(userName, userGroup);
        String newEntityValue = "newEntityValue";
        boolean isCreated = entityService.createEntity(user, entityName, entityValue);

        Entity createdEntity = repository.get(entityName);

        assertAll(
                () -> assertTrue(isCreated),
                () -> assertTrue(entityService.updateEntity(user, entityName, newEntityValue)),
                () -> assertEquals(newEntityValue, createdEntity.getValue()),
                () -> assertTrue(createdEntity.getGroups().containsAll(user.getGroups())),
                () -> assertFalse(createdEntity.getGroups().size() > user.getGroups().size())
        );
    }

    @Test
    void updateEntityManyUsersTest() {
        String groupName = "group";
        List<Permission> permissions = new ArrayList<>();
        permissions.add(Permission.read);
        permissions.add(Permission.write);

        String ownerName = "username";
        String simpleUserName = "simpleusername";

        String entityName = "entityName";
        String entityValue = "entityValue";

        Group userGroup = userService.createGroup(groupName, permissions);
        User owner = userService.createUser(ownerName, userGroup);
        User simpleUser = userService.createUser(simpleUserName, userGroup);
        String newEntityValue = "newEntityValue";
        boolean isCreated = entityService.createEntity(owner, entityName, entityValue);

        Entity createdEntity = repository.get(entityName);

        assertAll(
                () -> assertTrue(userGroup.getUsers().contains(simpleUser)),
                () -> assertTrue(userGroup.getUsers().contains(owner)),
                () -> assertTrue(isCreated),
                () -> assertTrue(entityService.updateEntity(simpleUser, entityName, newEntityValue)),
                () -> assertEquals(newEntityValue, createdEntity.getValue())
        );
    }
}
