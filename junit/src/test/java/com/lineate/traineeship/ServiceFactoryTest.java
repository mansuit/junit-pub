package com.lineate.traineeship;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ServiceFactoryTest {
    static ServiceFactory factory;

    @BeforeAll
    static void init() {
        factory = new ServiceFactory();
    }

    @Test
    void createUserServiceTest() {
        UserService userService = factory.createUserService();
        assertNotNull(userService);
    }

    @Test
    void createEntityServiceTest() {
        EntityService entityService = factory.createEntityService();
        assertNotNull(entityService);
    }

    @Test
    void createEntityServiceWithRepositoryTest() {
        EntityRepository repository = null;
        EntityService entityService = factory.createEntityService(repository);
        assertNotNull(entityService);
    }
}