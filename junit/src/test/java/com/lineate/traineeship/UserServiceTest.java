package com.lineate.traineeship;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class UserServiceTest {
    static UserService userService;

    @BeforeAll
    static void init() {
        ServiceFactory factory = new ServiceFactory();
        userService = factory.createUserService();
    }

    @Test
    void createUserTest() {
        String userName = "username";
        String groupName = "group";
        List<Permission> permissions = new ArrayList<>();
        permissions.add(Permission.write);
        permissions.add(Permission.read);

        Group userGroup = userService.createGroup(groupName, permissions);
        User user = userService.createUser(userName, userGroup);

        assertAll(
                () -> assertNotNull(user),
                () -> assertEquals(user.getName(), userName),
                () -> assertTrue(user.getGroups().contains(userGroup))
        );
    }

    @Test
    void createGroupTest() {
        String groupName = "group";
        List<Permission> permissions = new ArrayList<>();
        permissions.add(Permission.write);
        permissions.add(Permission.read);

        Group createdGroup = userService.createGroup(groupName, permissions);

        assertAll(
                () -> assertNotNull(createdGroup),
                () -> assertEquals(createdGroup.getName(), groupName),
                () -> assertTrue(createdGroup.getPermissions().containsAll(permissions)),
                () -> assertTrue(permissions.containsAll(createdGroup.getPermissions()))
        );
    }
}