package com.lineate.traineeship;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class EntityTest {
    static Entity entity;
    static User user;
    static String entityValue;

    @BeforeAll
    static void init() {
        ServiceFactory factory = new ServiceFactory();
        EntityRepository repository = null;
        EntityService entityService = factory.createEntityService(repository);
        UserService userService = factory.createUserService();

        String userName = "username";
        String groupName = "group";
        String entityName = "entityname";
        entityValue = "entityValue";

        List<Permission> permissions = new ArrayList<>();
        permissions.add(Permission.write);
        permissions.add(Permission.read);

        Group group = userService.createGroup(groupName, permissions);
        user = userService.createUser(userName, group);
        entityService.createEntity(user, entityName, entityValue);

        entity = repository.get(entityName);
    }

    @Test
    void getOwnerTest() {
        assertEquals(user, entity.getOwner());
    }

    @Test
    void getNameTest() {
        String name = entity.getName();

        assertAll(
                () -> assertNotNull(name),
                () -> assertFalse(name.isEmpty()),
                () -> assertFalse(name.length() > 32),
                () -> assertFalse(name.contains(" "))
        );
    }

    @Test
    void getValueTest() {
        assertEquals(entityValue, entity.getValue());
    }

    @Test
    void setValueTest() {
        String newValue = "newvalue";

        entity.setValue(newValue);
        assertEquals(newValue, entity.getValue());
    }
}