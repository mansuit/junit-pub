package com.lineate.traineeship;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class UserTest {
    static User user;

    @BeforeAll
    static void init() {
        ServiceFactory factory = new ServiceFactory();
        UserService userService = factory.createUserService();

        String userName = "username";
        String groupName = "group";

        List<Permission> permissions = new ArrayList<>();
        permissions.add(Permission.write);
        permissions.add(Permission.read);

        Group group = userService.createGroup(groupName, permissions);
        user = userService.createUser(userName, group);
    }

    @Test
    void getNameTest() {
        String name = user.getName();

        assertAll(
                () -> assertNotNull(name),
                () -> assertFalse(name.isEmpty())
        );
    }

    @Test
    void getGroupsTest() {
        Collection<Group> groups = user.getGroups();

        assertAll(
                () -> assertNotNull(groups),
                () -> assertFalse(groups.isEmpty())
        );
    }
}